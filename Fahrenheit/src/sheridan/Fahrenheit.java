package sheridan;

/**
 * 
 * @author Brian Hadskis 000001494
 * 
 */

public class Fahrenheit {
	public static int fromCelsius(int value)
	{
		if (value <= -274)
			throw new IllegalArgumentException("Invalid value: below absolutge zero");
		double celsius = (double)value;
		double fahrenheit = ((9 * celsius) + (32 * 5)) / 5;
		
		return (int)Math.round(fahrenheit);
	}
}
