package sheridan;

/**
 * 
 * @author Brian Hadskis 000001494
 * 
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsius_Regular() {
		assertTrue("Celsius conversion invalid", Fahrenheit.fromCelsius(100) == 212);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFromCelsius_Exceptional() {
		Fahrenheit.fromCelsius(-500);
		fail("Celsius valus given was below absolute zero");
	}
	
	@Test
	public void testFromCelsius_BoundaryIn() {
		assertTrue("Celsius conversion invalid", Fahrenheit.fromCelsius(-273) == -459);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFromCelsius_BoundaryOut() {
		Fahrenheit.fromCelsius(-274);
		fail("Celsius valus given was below absolute zero");
	}

}
